# Stage 1

Combination of REST + HTTP = Internet and App Ecosystem

## Contents

- HTTP
- REST
- Linux
- AWS
- Git

---

# HTTP

### Intro to HTTP

**Primary goals**

- Get a clear understanding of how HTTP works.
- Use tools like cURL & Postman to perform HTTP requests and analyse responses

HTTP stands for HyperText Transfer Protocol. It is, like the name suggests, a set of rules for querying the web. It works on a client-server model, where the client, in most cases, the browser, makes a request, and waits for the server to respond.

Browsers use HTTP Requests to fetch us web pages. When we enter a website URL, the browser creates a HTTP Request on our behalf and sends it to the server on which the website is hosted. The HTTP Response from the server is read by the browser and rendered for us beautifully as web pages instead of the raw HTML returned. Let’s look into what constitutes a HTTP request & response.

HTTP is a ‘stateless protocol’, meaning two corresponding requests do not share data - your prior request is not ‘remembered’ in any way by the following one - this obviously has some flip sides - you might need to keep resending data that you want to persist through requests - why is it still designed this way?

### HTTP Request Methods

- GET - Read from server
- POST - Write on server
- PUT - Update existing with the new data on server
- Other HTTP methods are HEAD, DELETE, OPTIONS, CONNECT, TRACE & PATCH

### HTTP Status Code

HTTP Status codes are part of the HTTP Response.

It helps the client understand what happened to the request. Status codes are 3 digit numbers (201, 304 etc) and are categorised to 5 different families based on their starting digit.

Along with the status code, a Reason-Phrase is also present (OK, Moved Permanently etc) which gives a short description of the status code. The Status Code is intended for machines whereas Reason-Phrase is for humans.

- Status code - 1xx - for information purposes
- Status codes - 2xx - status codes 200-299 signifies the HTTP request was successfully received & understood by the server.
- Status codes - 3xx - denotes that further action must be taken to complete the HTTP request made.
- Status codes - 4xx - tells us that there was an error in the HTTP request sent by the client
- Status code - 5xx - signifies there was a server error

### Using cURL & Postman

- Install Git
- Install cURL Utility for HTTP requests
- Install Postman API tool

### Composing an HTTP request

The telnet client helps us connect to other computers on the internet. The format is telnet hostname port. (You can use this online telnet client)

Opening a TCP connection via `telnet` - The default port for HTTP is 80 and the telnet command has us connected to the HTTP port on the data.pr4e.org server.

From the above figure, different parts of the HTTP communication are:

- Request Line (HTTP Request)
- Status Line & Response Header (HTTP Response)
- Response Body (HTTP Response)

### Interview Corner

What is HTTP? Why was it introduced?

What happens when you type google.com in a browser?

What are the main HTTP verbs/methods?

What are the different HTTP response codes?

What is the difference between HTTP and HTTPS?

What is cURL?

What is Postman used for?

---

# REST

**Primary goals**

- Understand what REST APIs are
- Understand how to make REST API calls

### Getting Started with REST APIs

REST stands for REpresentational State Transfer

API stands for Application Programming Interface

APIs have different styles, or in more formal terms - conventions and architectures about how they are used.

REST APIs are those APIs which follow the guidelines set by the REST architecture. They follow a client-server model where one software program sends a request and the other responds with some data. REST APIs commonly use the HTTP protocol to send requests & receive responses.

HTTP requests for webpages return HTML, CSS & JavaScript files which are rendered by the browser and displayed to the user. But, in the case of APIs, the request can be for any data (not just webpage) and JSON (JavaScript Object Notation) is a standard format that is easily "understandable" by applications and can be handled well in most languages.

Wireshark is a popular network analysis tool to capture network packets and display them at a granular level. Once these packets are broken down, you can use them for real-time or offline analysis.

### Anatomy of REST APIs

The purpose of having an API is for applications to communicate with each other

APIs makes it easier for

- Applications to expose their services
- Other applications to avail those services.
- Integration is easier, only the API definitions need to be exposed.

Install the JSONView Chrome extension

How do I pass multiple query parameters with a single URL?

- Search parameters come after the ? character. Multiple search parameters can be used by separating them with the & character.
- For an API that needs to take two search parameters - query(city) and location (country), the request URL should be like this `https://www.metaweather.com/api/location/search/?query=san&location=India`

### REST calls using Programs

REST API calls earlier are made using Web Browser and now see how to do that programmatically.

- Using cURL

  ```json
  [{"title":"San Francisco","location_type":"City","woeid":2487956,"latt_long":"37.777119, -122.41964"},{"title":"San Diego","location_type":"City","woeid":2487889,"latt_long":"32.715691,-117.161720"},{"title":"San Jose","location_type":"City","woeid":2488042,"latt_long":"37.338581,-121.885567"},{"title":"San Antonio","location_type":"City","woeid":2487796,"latt_long":"29.424580,-98.494614"},{"title":"Santa Cruz","location_type":"City","woeid":2488853,"latt_long":"36.974018,-122.030952"},{"title":"Santiago","location_type":"City","woeid":349859,"latt_long":"-33.463039,-70.647942"},{"title":"Santorini","location_type":"City","woeid":56558361,"latt_long":"36.406651,25.456530"},{"title":"Santander","location_type":"City","woeid":773964,"latt_long":"43.461498,-3.810010"},{"title":"Busan","location_type":"City","woeid":1132447,"latt_long":"35.170429,128.999481"},{"title":"Santa Cruz de Tenerife","location_type":"City","woeid":773692,"latt_long":"28.46163,-16.267059"},{"title":"Santa Fe","location_type":"City","woeid":2488867,"latt_long":"35.666431,-105.972572"}]s
  ```

- Using Python

  ```python
  import requests # Import a library that allows to make HTTP request
  url = "https://www.metaweather.com/api/location/search/?query=san"
  response = requests.get(url)
  print(response.text)
  ```

- Using Java
  ```java
  import java.io.BufferedReader;
  import java.io.IOException;
  import java.io.InputStreamReader;
  import java.net.HttpURLConnection;
  import java.net.MalformedURLException;
  import java.net.URL;
  public class Main {
      public static void main(String[] args) throws MalformedURLException, IOException {
          // create url
          URL url = new URL("https://www.metaweather.com/api/location/search/?query=san");
          // Send Get request and fetch data
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setRequestMethod("GET");
          BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
          // Read data line-by-line from buffer & print it out
          String output;
          while ((output = br.readLine()) != null) {
              System.out.println(output);
          }
          conn.disconnect();
      }
  }
  ```

### LinkedIn with REST API

Copy the cURL after posting on LinkedIn and run in the terminal

### Interview Corner

What’s REST and how it differs from GraphQL?

What’s an API, how do you ‘hit’ an API? How do you query APIs?

What are three things you’ll keep in mind when creating a REST API?

---

# Linux

When you create your own Linux virtual machine (VM) from services like GCloud, AWS or Microsoft Azure, you don’t usually get access to a Graphical User Interface (GUI). You have to use the Linux terminal to operate and manage your VM.

Some of the advantages of the Linux terminal include:

- The ability to create and run scripts in several languages (Bash, Python, Perl, and more) right from the command line.
- Easily automate several workflows that are much harder to do in a GUI.
- The network bandwidth required to access a Linux system via terminal is far lesser than that required by a GUI.

**Primary goals**

- Get familiar with the Linux terminal.
- Learn the commands necessary to navigate directories and files.
- Perform file management operations - create, read, update and delete.
- Search directories for files and search files for interesting patterns.
- Learn output redirection where you send the output of a command to a file (or as the input to a different command)
- Performing Data Analysis of Hadoop log files right from the terminal and realize the power of Linux.

### The Linux Terminal

In Linux, directories are separated by the forward slash `/`. This is different from Windows where the backslash `\` is used to separate directories.

APT is a packet manager in Linux.

Commands

- `pwd` - present working directory
- `ls` - list items
- `cd` - change directory
- `cd ~` - will take you to your home directory.
- `#` - The pound sign used to write comments. Everything after the first # is ignored
- `history` - prints out a list of the previous commands executed.
- `tab` key - pressing the tab key can be used to auto-complete the directory and file names while typing paths or filenames

### File Systems & files

Irrespective of the present working directory, you can run `cd /var` to take you to `/var`. Similarly, `ls /var` can be executed from any working directory and it will always display the contents of `/var.`

Absolute Path - `cd /var`
Relative Path

- `.` - present working directory
- `..` - parent directory
- `-` - previously working directory

Linux commands can be tuned to our requirements by providing flags along with the command when calling them. These are usually a hyphen (-) followed by an alphabet eg: `-a`, `-B` etc or double-hyphen (--) followed by text eg: `--all`, `--color`

Commands come with a "Manual" as well. We can access it using the `man` command followed by the name of the command we need to see the manual of. For ls, we do `man ls`.

Stuck inside the manual page? - Hit `q` to exit

#### Manipulating file permissions

Executables are programs that can be run to perform some instructions.

```bash
echo "echo 'Congratulations on running a bash script'" > run.sh
```

To run bash files, use `./<name-of-executable>` like `./run.sh`

File permissions

- `r` - read
- `w` - write
- `x` - executable

`chmod` command lets us change the access mode of a file. To add executable permission, we use `chmod +x <filename>` like `chmod +x run.sh`

`ls -R` : Use the ls command to get recursive directory listing on Linux

`find /dir/ -print` : Run the find command to see recursive directory listing in Linux

`du -a .` : Execute the du command to view recursive directory listing on Unix

`ls -l` lists the contents in long form

`ls -t` sorts the files according to their modification time.

`ln` command is used to create for symbolic link

### Dealing with files & directories

`mkdir` is the command we need to make directories

`nano` command lets us edit files and is one of the default editors on Ubuntu

`cp` command is used for copyfiles as `cp <filename> <location>` such as`cp part1/cast.txt .`

`rm` is used to delete the file as `rm <filename>` such as `rm part1/cast.txt`

Use the `tree` command now to verify the structure again

NOTE: Everything is a file in Linux ([Refer](https://www.tecmint.com/explanation-of-everything-is-a-file-and-types-of-files-in-linux/))

`rm -rf <filename>` OR `rmdir <filename>` deletes the folders

`mv oldfile Newfile` renames the name of the file

### Manupilating files and output redirection

`echo <message>` command is used to print out the value provided to it

We can redirect the output instead to a file using the redirection operator `>`

```
echo "echo 'Congratulations on running a bash script'" > run.sh
```

`grep` command is used to filter/search for text using strings or patterns. It’s usage is `grep <pattern> <file>`

`awk` only used for seaching numerics. It uses space as a field separator (by default space is used as field separator) and separates text into columns.

Piping is a type of redirection in Linux used to send output of one command to input of another command.

`>` redirect the output and writes it to a file. Try running `echo "Hello" > hello.txt`

`tail` command displays file getting printed to screen as and when the file is being written to

### Data Analysis from the command line

Scripts and/or automation is their answer to these repetitive tasks.

`cron` command to schedule execution of this script

The Cron runs processes on your system at a scheduled time. Cron reads the crontab (cron tables) for predefined commands and scripts that need to be run.

By using a specific syntax, you can configure a cron job which schedules scripts or other commands to run automatically.

```bash
# Open the cron tables with this command
crio-user:~/workspace$ crontab -e
# This will open the crontab file in an editor. Choose nano if prompted for choice of editor.
# Go the last line of this file and add the below line
5 * * * * /home/crio-user/workspace/hadoop_log_analysis.sh /tmp/log_analysis > /home/crio-user/workspace/log_report
# Substitute the above paths to suit your setup
# Save file and exit  ("Ctrl X", followed by “Y”, followed by “Enter” to exit nano editor)
```

The script can be enhanced to send a text message or an email when a particular value in the report is not as expected. For example, when too many FATAL messages are seen.

Other example tasks you could accomplish using cron jobs

- Backup files or database periodically
- Cleanup disk space when it fills up beyond a certain limit
- Monitor processes and restart them if they go down

[Linux Usage](https://tldp.org/HOWTO/VMS-to-Linux-HOWTO/examples.html)

[Detailed Linux Commands](https://goalkicker.com/LinuxBook/)

---

# AWS

Cloud is a bunch of hardware resources like storage or compute that someone maintains for you.

Benefits of Cloud Computing

- Scalable
- Instant
- Save money
- More focus on work than on server hardware

Some of the popular offerings from AWS are:

- Virtual machines (EC2): VM - in simple terms, computers with some configuration; eg: 2cpu, 16GB RAM, 20 GB hard disk
- Storage (S3)
- Load Balancers (ELB)

Docker - It is a containerization technology - you can think of them as tiny VMs.

**Primary goals**

- Launch your first virtual server in AWS
- Deploy the app backend server
- Connect mobile app to the app backend server

### AWS Account Setup & Create an EC2 Instance

Never forget to stop your instance when you’re going off, to avoid unexpected costs. You can always restart it.

### SSH into your instance

For Windows users only, before we go ahead, we need PuTTY - a terminal program to be able to SSH.

SSH(Secure Socket Shell) is a network protocol that allows you to sign in to your servers from a remote computer.

```bash
# Usually downloads go to ~/Downloads/
chmod 400 <path-to-your-pem-file>
ssh -i "crio-demo.pem" ubuntu@<your-ec2-url>
# Example
ssh -i "~/Downloads/crio-demo.pem" ubuntu@ec2-13-233-100-101.ap-south-1.compute.amazonaws.com
```

### Deploying app backend server

**Installing Docker**

```bash
sudo apt-get update
sudo apt-get install wget
wget -qO- https://get.docker.com/ | sh
sudo usermod -aG docker $USER
sudo service docker start
newgrp docker
```

**Installing telnet**

Telnet is a network protocol used to virtually access a computer and to provide a two-way, collaborative and text-based communication channel between two machines.

More about Telnet [here](https://searchnetworking.techtarget.com/definition/Telnet)

```bash
sudo apt install -y telnet
```

Running the docker container for QEats app server

```bash
sudo docker run -d -m 800m -v /tmp/container:/tmp:rw -p 8081:8081 criodo/qeats-server
```

`telnet localhost 8081` to make the connection and `Ctrl + ]` to return back to terminal

To logout of the server

```bash
logout
```

### Connect App to the server

[https://www.genymotion.com/](https://www.genymotion.com/) is an online Android emulator to run apps online without installing on the phone

### Interview Corner

What is EC2?

What is an AMI(Amazon Machine Image)?

What are some different types of instances that AWS offers? What do they differ in?

How do you connect to a remote machine?

What is SSH?

What is Docker?

---

# Git

Version control systems track of changes to our files by taking snapshots of them.

We can look at earlier snapshots and also restore our code to an earlier snapshot. This lets us work on our projects without worrying about introducing errors to partly/completely working versions and not being able to go back.

Git is one of them, probably the most popular one.

Similar to having Google Drive for file sharing, we have version control providers like GitLab, GitHub etc. to facilitate the use of Git repositories.

Primary goals

- Understand how Git works
- Learn the basic Git commands

[Git Cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)
